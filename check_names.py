import os
import argparse


class FileList:
    def __init__(self, user_path, length):
        self.my_files = self.grab_list_of_files(user_path)
        self.length = length
        self.user_path = user_path

    def __repr__(self):
        return f"A fileset based on {self.user_path}"

    @staticmethod
    def grab_list_of_files(path):
        for i in os.walk(path):
            return [file for file in i[2]]

    def full_check(self):
        self.print_bad_files(self.check_for_spaces(), "spaces")
        self.print_bad_files(self.check_length(), "the wrong number of series of digits")
        self.print_bad_files(self.check_length_of_series(), "the wrong length of series based on our practices")

    @staticmethod
    def print_bad_files(bad_files, message):
        if len(bad_files) > 0:
            print(f"\nThese files have {message}:\n")
            i = 1
            for file in bad_files:
                print(f"\t{i}. {file}")
                i += 1
            return

    def check_for_spaces(self):
        return [file for file in self.my_files if file.find(" ") is not -1]

    def check_length(self):
        return [file for file in self.my_files if len(file.split("_")) != self.length]

    def check_length_of_series(self):
        bad_files = []
        for file in self.my_files:
            current_file = file.split("_")
            if len(current_file[0]) is not 4:
                bad_files.append(file)
            elif len(current_file[1]) is not 6:
                bad_files.append(file)
            elif self.length is 3 and len(current_file[2]) is not 10:
                bad_files.append(file)
            elif self.length is 4 and len(current_file[2]) is not 6:
                bad_files.append(file)
            elif self.length is 4 and len(current_file[3]) is not 8:
                bad_files.append(file)
        return bad_files


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Enter your file path')
    parser.add_argument("-p", "--path", dest="path", help="Specify the path to your files.")
    parser.add_argument("-f", "--fluffy_path", dest="fluffy", help="If on Fluffy, which collection to test?")
    parser.add_argument("-l", "--length", dest="length", help="3 or 4 series file names", default=3)
    args = parser.parse_args()
    if args.fluffy:
        my_path = f"/d1/area3/prod3/{args.fluffy}/delivery"
    else:
        my_path = args.path
    my_length = float(args.length)
    my_set = FileList(my_path, my_length)
    my_set.full_check()
