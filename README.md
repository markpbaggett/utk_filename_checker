# File Name Checker

---

## Examples

Check filenames in the delivery directory on Fluffy according to UTK standards:

```
python check_names.py -f Yearbooks -l 4
```

Check filenames more specifically.

```
python check_names.py -p /d1/area3/prod3/Yearbooks -l 3
```